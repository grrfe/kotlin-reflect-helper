plugins {
    java
    `maven-publish`
    kotlin("jvm") version "1.9.24"
    id("net.nemerosa.versioning") version "3.1.0"
}

group = "fe.kotlin-reflect-helper"
version = versioning.info.tag ?: versioning.info.full

repositories {
    mavenCentral()
}

dependencies {
    api(kotlin("reflect"))
    testImplementation(kotlin("test"))
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = project.group.toString()
            version = project.version.toString()

            from(components["java"])
        }
    }
}
