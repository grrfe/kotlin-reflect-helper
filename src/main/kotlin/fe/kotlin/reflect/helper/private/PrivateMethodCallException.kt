package fe.kotlin.reflect.helper.private

import kotlin.reflect.KClass

sealed class PrivateMethodCallException(message: String) : Exception(message) {
    class PrivateMethodNotFound(
        method: String,
        classInstance: KClass<*>
    ) : PrivateMethodCallException("Failed to find $method on $classInstance")

    class DifferentParameterLength(
        method: String,
        methodParamterCount: Int,
        inputParameterCount: Int
    ) : PrivateMethodCallException("$method requires $methodParamterCount parameters, while we only have $inputParameterCount")

    class PrivateMethodDoesNotHaveKey(
        method: String,
        key: String
    ) : PrivateMethodCallException("$method does not have a parameter named $key")
}


fun privateMethodNotFound(
    method: String,
    classInstance: KClass<*>
): Nothing = throw PrivateMethodCallException.PrivateMethodNotFound(method, classInstance)

fun differentParameterLength(
    method: String,
    methodParamterCount: Int,
    inputParameterCount: Int
): Nothing = throw PrivateMethodCallException.DifferentParameterLength(method, methodParamterCount, inputParameterCount)

fun privateMethodDoesNotHaveKey(
    method: String,
    key: String
): Nothing = throw PrivateMethodCallException.PrivateMethodDoesNotHaveKey(method, key)
