package fe.kotlin.reflect.helper.private

import kotlin.jvm.Throws
import kotlin.reflect.KClass
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.functions
import kotlin.reflect.jvm.isAccessible

@Suppress("UNCHECKED_CAST")
abstract class PrivateMethodCaller<T : PrivateMethodCaller<T, I, R>, I : Any, R>(
    private val callOnInstance: I,
    private val methodName: String
) {
    private val inst by lazy { this as T }
    private val classInstance by lazy { inst::class as KClass<T> }

    @Throws(PrivateMethodCallException::class)
    operator fun invoke(): R {
        val privateMethod = callOnInstance::class.functions.firstOrNull { it.name == methodName }
            ?.apply { isAccessible = true } as? KFunction<R>
            ?: privateMethodNotFound(methodName, classInstance)

        var instanceParam: KParameter? = null
        val methodParams = privateMethod.parameters.filterIndexed { index, param ->
            // if kind == VALUE, this is an actual property we need to pass, if it isn't then it's either an INSTANCE
            // or EXTENSION_RECEIVER (usually only on parameter[0]).
            val valueParam = param.kind == KParameter.Kind.VALUE
            if (!valueParam && index == 0) instanceParam = param

            valueParam
        }.associateBy { it.name }

        val methodCallMap = linkedMapOf<KParameter, Any?>()
        // Instance parameters must be passed as the first parameter to the callBy method
        if (instanceParam != null) methodCallMap[instanceParam!!] = callOnInstance

        val fieldValues = classInstance.declaredMemberProperties.associate { it.name to it.get(inst) }
        if (methodParams.size != fieldValues.size) differentParameterLength(
            methodName,
            methodParams.size,
            fieldValues.size
        )

        val parameterToValueMap = fieldValues.map { (key, value) ->
            val kproperty = methodParams[key] ?: privateMethodDoesNotHaveKey(methodName, key)
            kproperty to value
        }.toMap(methodCallMap)

        return privateMethod.callBy(parameterToValueMap)
    }
}
